package com.deyi.govaffair;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * @author admin
 */
@SpringBootApplication
public class QuartzConfigClusterApplication {
	public static void main(String[] args) {
		SpringApplication.run(QuartzConfigClusterApplication.class, args);

		System.out.println("【【【【【【 Quartz-Config-Cluster微服务 】】】】】】已启动.");
	}
}


